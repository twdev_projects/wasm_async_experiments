# WASM - JS interop

This repo demonstrates how to integrate WASM and JS blocking code.

[Click here](https://wasm-async-experiments-twdev-projects-08da1525ad78ab970e0709b2c.gitlab.io/) to play with the demo page.
