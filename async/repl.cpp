#include <cstdio>
#include <cstring>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#ifdef __EMSCRIPTEN__

EM_ASYNC_JS(char *, em_fgets, (const char* buf, size_t bufsize), {
  return await new Promise((resolve, reject) => {
      if (Module.pending_lines.length > 0) {
        resolve(Module.pending_lines.shift());
      } else {
        Module.pending_fgets.push(resolve);
      }
  }).then((s) => {
      // convert JS string to WASM string
      let l = s.length + 1;
      if (l >= bufsize) {
        // truncate
        l = bufsize - 1;
      }
      Module.stringToUTF8(s.slice(0, l), buf, l);
      return buf;
  });
});

#endif

void doRepl() {
  constexpr std::size_t MAXBUF = 100;
  char buffer[MAXBUF];
  char* p = buffer;

#ifdef __EMSCRIPTEN__
  p = em_fgets(buffer, MAXBUF);
#else
  p = fgets(buffer, MAXBUF, stdin);
#endif

  if (!p) {
      printf("Error reading input\n");
  }

  printf("Echo: %s\n", buffer);
}

int main(int argc, const char *argv[]) {
  printf("I'm an echo\n");

#ifdef __EMSCRIPTEN__
  int fps = 0;
  int simulate_infinite_loop = 1;
  emscripten_set_main_loop(doRepl, fps, simulate_infinite_loop);
#else
  while (true) {
    doRepl();
  }
#endif

  return 0;
}
