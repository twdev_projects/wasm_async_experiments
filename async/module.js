var Module = {
  pending_fgets : [],
  pending_chars : [],
  pending_lines : [],
  print : (text) => {
    const output_element = document.getElementById('output');
    output_element.textContent += text + '\n';
  },
};
