var Module = {
  pending_input : [],
  print : (text) => {
    const output_element = document.getElementById('output');
    output_element.textContent += text + '\n';
  },
};
