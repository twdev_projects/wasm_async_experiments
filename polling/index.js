const input_element = document.getElementById('input');

input_element.addEventListener('keypress', (e) => {
    if (e.key === 'Enter') {
        e.preventDefault();
        Module.pending_input.push('\n'.charCodeAt(0));
        input_element.value = '';
    } else {
        Module.pending_input.push(e.key.charCodeAt(0));
    }
});
