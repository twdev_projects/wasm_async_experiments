#include <cstdio>
#include <cstring>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#ifdef __EMSCRIPTEN__

EM_JS(int, js_getchar, (), { 
    if (Module.pending_input.length == 0) {
      return 0;
    }
    return Module.pending_input.shift();
});

char *em_fgets(char *buf, std::size_t size, FILE *stream) {
  while (true) {     
    if (int c = js_getchar()) {
      if (c == '\n') {           
        *buf = '\0';                       
        return buf;                                                                         
      } else {                                                                              
        if (size == 1) {
          *buf = '\0';
          return buf;
        }
        *buf++ = c;
        size--;
      }                              
      continue;
    }
    emscripten_sleep(100);                                                                                                                                                              
  }

  return NULL;
}

#endif

void doRepl() {
  constexpr std::size_t MAXBUF = 100;
  char buffer[MAXBUF];
  char* p = buffer;

#ifdef __EMSCRIPTEN__
  p = em_fgets(buffer, MAXBUF, stdin);
#else
  p = fgets(buffer, MAXBUF, stdin);
#endif

  if (!p) {
      printf("Error reading input\n");
  }

  printf("Echo: %s\n", buffer);
}

int main(int argc, const char *argv[]) {
  printf("I'm an echo\n");

#ifdef __EMSCRIPTEN__
  int fps = 0;
  int simulate_infinite_loop = 1;
  emscripten_set_main_loop(doRepl, fps, simulate_infinite_loop);
#else
  while (true) {
    doRepl();
  }
#endif

  return 0;
}
