const input_element = document.getElementById('input');

input_element.addEventListener('keypress', function(e) {
  const isEnter = e.key === 'Enter';

  if (isEnter) {
    e.preventDefault();
    Module.pending_lines.push(Module.pending_chars.join(''));
    Module.pending_chars = [];
    input_element.value = '';
  } else {
    Module.pending_chars.push(e.key);
  }

  if (Module.pending_fgets.length > 0 && Module.pending_lines.length > 0) {
    let resolver = Module.pending_fgets.shift();
    resolver(Module.pending_lines.shift());
  }
});
